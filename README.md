![Tetros](https://i.imgur.com/OdZUEzH.png)

An LWJGL implementation of Tetris.

![Imgur](https://i.imgur.com/Znq4EbF.png)

## Setting up LWJGL

Run the [set_up_lwjgl.sh](set_up_lwjgl.sh) script

```sh
$ sh set_up_lwjgl.sh
```

Or download version 2.9.3 manually from the [LWJGL website](http://legacy.lwjgl.org/download.php.html) and create a folder `lib` to extract the zip file into.

## Building

### Using IntelliJ IDEA

Open the project in [IntelliJ IDEA](https://www.jetbrains.com/idea/) and navigate to Build -> Build Artifacts -> Tetros.jar -> Build.

The generated jar file does not include the native binaries for LWJGL. Therefore the jar can be executed with

```sh
$ java -Djava.library.path=<path-to-binaries> Tetros.jar
```

The native binaries can be found in the LWJGL directory.

**Warning**: IntelliJ is currently set up to only use the Linux natives in its build process. To change this, navigate to File -> Project Structure -> Project Settings -> Libraries -> lwjgl and remove the Linux natives (under *Native Library Location* using the minus sign) and add the desired natives (using the plus sign).
