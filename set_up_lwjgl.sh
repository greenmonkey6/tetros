#!/bin/sh

folder=lib
file=/tmp/lwjgl.zip

# Downloads the jar to the library folder
mkdir -p "$folder"
wget https://datapacket.dl.sourceforge.net/project/java-game-lib/Official%20Releases/LWJGL%202.9.3/lwjgl-2.9.3.zip -O "$file"
unzip "$file" -d "$folder"
rm "$file"
