import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;


/**
 * Ein Button ist ein Knopf mit Beschriftung auf der graphischen Oberflaeche mit dem ueber die Maus interagiert werden
 * kann.
 *
 * @author Tobias Schleifstein
 */
public class Button {
	/**
	 * Boundingbox des Buttons.
	 */
	private double x1, y1, x2, y2;

	/**
	 * Gibt an, ob sich die Maus auf dem Button befindet.
	 */
	private boolean selected;

	/**
	 * Gibt an, ob der Button gedrueckt ist.
	 */
	private boolean pressed;

	/**
	 * Gibt an, ob der Button geklickt ist, also gedrueckt und wieder losgelassen wurde.
	 */
	private boolean clicked;

	/**
	 * Titel des Buttons.
	 */
	private String title;

	/**
	 * Gibt an, ob der Button gedrueck werden kann.
	 */
	private boolean pressable;

	/**
	 * Transparenz des Buttons.
	 */
	private double alpha;

	/**
	 * Initialisiert den Button.
	 */
	public Button() {
		// Button standardmaessig 'drueckbar' machen
		pressable = true;

		// Titel des Buttons initialisieren
		title = "";

		// Button standardmaessig sichtbar machen
		alpha = 1;
	}

	/**
	 * Aktualisiert den Button und regiert auf die Maus.
	 */
	public void update() {
		// Ueberpruefen, ob der Button gedrueckt werden kann
		if (pressable) {
			// Ueberpruefen, ob sich die Maus auf dem Button befindet
			if (Mouse.getX() >= x1 && Mouse.getX() <= x2 && Mouse.getY() >= y1 && Mouse.getY() <= y2) {
				selected = true;
			} else {
				selected = false;
				pressed = false; // Fuer den Fall, dass der Button ohne Loslassen des Mausbuttons verlassen wird
			}

			if (selected) {
				// Ueberpruefen, ob die Maus gedrueckt ist
				if (Mouse.isButtonDown(0)) {
					pressed = true;
				} else {
					// Wenn der Button losgelassen wird
					if (pressed) {
						clicked = true;
					}

					pressed = false;
				}
			}
		}
	}

	/**
	 * Rendert den Button.
	 */
	public void render() {
		// Setzt die Farbe entsprechend des aktuellen Zustands des Buttons
		if (selected) {
			if (pressed) {
				GL11.glColor4d(Main.GRAY_R, Main.GRAY_G, Main.GRAY_B, alpha);
			} else {
				GL11.glColor4d(Main.GRAY_R - 0.2, Main.GRAY_G - 0.2, Main.GRAY_B - 0.2, alpha);
			}
		} else {
			GL11.glColor4d(Main.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B, alpha);
		}

		// Zeichnet Button
		GL11.glRectd(x1, y1, x2, y2);

		double fontSize = (y2 - y1) * 0.7;
		Font.renderString(title, (x1 + x2 - Font.getRenderedStringLength(title, fontSize)) * 0.5, (y1 + y2 - fontSize)
				* 0.5, fontSize, Main.VERY_LIGHT_GRAY_R, Main.VERY_LIGHT_GRAY_G, Main.VERY_LIGHT_GRAY_B, alpha);
	}

	/**
	 * Gibt zurueck, ob die Maus ueber dem Button ist.
	 *
	 * @return Gibt an, ob die Maus ueber dem Button ist.
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Gibt zurueck, ob der Button gedrueckt ist.
	 *
	 * @return Gibt an, ob der Button gedrueckt ist.
	 */
	public boolean isPressed() {
		return pressed;
	}

	/**
	 * Gibt zurueck, ob der Button geklickt ist und setzt den Wert danach auf
	 * false.
	 *
	 * @return Gibt an, ob der Button geklickt ist.
	 */
	public boolean isClicked() {
		boolean temp = clicked;
		clicked = false;

		return temp;
	}

	/**
	 * Setzt, ob der Button gedrueckt werden kann.
	 *
	 * @param pressable Gibt an, ob der Button gedrueckt werden koennen soll.
	 */
	public void setPressable(boolean pressable) {
		this.pressable = pressable;

		if (!pressable) {
			pressed = false;
		}
	}

	/**
	 * Gibt zurueck, ob der Button gedrueckt werden kann.
	 *
	 * @return Gibt an, ob der Button gedrueckt werden kann.
	 */
	public boolean isPressable() {
		return pressable;
	}

	/**
	 * Setzt die Boundingbox des Buttons
	 *
	 * @param x1 x-Koordinate der linken unteren Ecke.
	 * @param y1 y-Koordinate der linken unteren Ecke.
	 * @param x2 x-Koordinate der rechten oberen Ecke.
	 * @param y2 y-Koordinate der rechten oberen Ecke.
	 */
	public void setBounds(double x1, double y1, double x2, double y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}

	/**
	 * Gibt die x-Koordinate der linken unteren Ecke des Buttons zurueck.
	 *
	 * @return x-Koordinate der linken unteren Ecke des Buttons.
	 */
	public double getX1() {
		return x1;
	}

	/**
	 * Gibt die y-Koordinate der linken unteren Ecke des Buttons zurueck.
	 *
	 * @return y-Koordinate der linken unteren Ecke des Buttons.
	 */
	public double getY1() {
		return y1;
	}

	/**
	 * Gibt die x-Koordinate der rechten oberen Ecke des Buttons zurueck.
	 *
	 * @return x-Koordinate der rechten oberen Ecke des Buttons.
	 */
	public double getX2() {
		return x2;
	}

	/**
	 * Gibt die y-Koordinate der rechten oberen Ecke des Buttons zurueck.
	 *
	 * @return y-Koordinate der rechten oberen Ecke des Buttons.
	 */
	public double getY2() {
		return y2;
	}

	/**
	 * Setzt Titel des Buttons.
	 *
	 * @param title Titel des Buttons.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Setzt Transparanz des Buttons.
	 *
	 * @param alpha Transparanz (Alpha-Wert).
	 */
	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}
}
