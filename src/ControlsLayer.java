import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;


/**
 * Der ControllsLayer ist der Layer im Spiel, welcher zum Aendern der Tasten, welche im eigentlichen Spiel benutzt
 * werden, zustaendig ist. Es koennen die Tasten fuer das Bewegen eines Steins, sowie dessen Rotation geaendert werden.
 *
 * @author Tobias Schleifstein
 */
public class ControlsLayer extends Layer {
	/**
	 * Taste fuer die Bewegung nach unten.
	 */
	public static int keyMoveDown;

	/**
	 * Taste fuer die Bewegung nach links.
	 */
	public static int keyMoveLeft;

	/**
	 * Taste fuer die Bewegung nach rechts.
	 */
	public static int keyMoveRight;

	/**
	 * Taste fuer die Rotation nach links.
	 */
	public static int keyRotateLeft;

	/**
	 * Taste fuer die Rotation nach rechts.
	 */
	public static int keyRotateRight;

	/**
	 * Button fuer die Bewegung nach unten.
	 */
	private KeyButton buttonMoveDown;

	/**
	 * Button fuer die Bewegung nach links.
	 */
	private KeyButton buttonMoveLeft;

	/**
	 * Button fuer die Bewegung nach rechts.
	 */
	private KeyButton buttonMoveRight;

	/**
	 * Button fuer die Rotation nach links.
	 */
	private KeyButton buttonRotateLeft;

	/**
	 * Button fuer die Rotation nach rechts.
	 */
	private KeyButton buttonRotateRight;

	/**
	 * Button, um ins Menue zurueckzukehren
	 */
	private Button buttonBackToMenu;

	/**
	 * Array mit allen Buttons.
	 */
	private Button[] buttons;

	/**
	 * Menue, in welches zurueckgewechselt wird.
	 */
	private MenuLayer menuLayer;

	@Override
	public void init() {
		// Initialisiert Buttons
		buttonMoveDown = new KeyButton();
		buttonMoveDown.setTitle("MOVE DOWN");
		buttonMoveDown.setKey(keyMoveDown);

		buttonMoveLeft = new KeyButton();
		buttonMoveLeft.setTitle("MOVE LEFT");
		buttonMoveLeft.setKey(keyMoveLeft);

		buttonMoveRight = new KeyButton();
		buttonMoveRight.setTitle("MOVE RIGHT");
		buttonMoveRight.setKey(keyMoveRight);

		buttonRotateLeft = new KeyButton();
		buttonRotateLeft.setTitle("ROTATE LEFT");
		buttonRotateLeft.setKey(keyRotateLeft);

		buttonRotateRight = new KeyButton();
		buttonRotateRight.setTitle("ROTATE RIGHT");
		buttonRotateRight.setKey(keyRotateRight);

		buttonBackToMenu = new Button();
		buttonBackToMenu.setTitle("BACK TO MENU");

		buttons = new Button[6];
		buttons[0] = buttonMoveDown;
		buttons[1] = buttonMoveLeft;
		buttons[2] = buttonMoveRight;
		buttons[3] = buttonRotateLeft;
		buttons[4] = buttonRotateRight;
		buttons[5] = buttonBackToMenu;

		// Alles richtig skalieren
		scale();
	}

	@Override
	public void update(long delta) {
		// Buttons updaten
		for (int i = 0; i < buttons.length; i++) {
			buttons[i].update();
		}

		// Geht Buttons durch
		for (int i = 0; i < buttons.length; i++) {
			// KeyButtons
			if (buttons[i] instanceof KeyButton) {
				KeyButton button = (KeyButton) buttons[i];

				// Taste von Button loeschen
				if (button.isClicked()) {
					button.setKey(Keyboard.CHAR_NONE);

					// Tastaturspeicher leeren
					while (Keyboard.next()) {
						// nichts
					}
				}

				// Button ohne Taste, Taste zuweisen
				if (button.getKey() == Keyboard.CHAR_NONE) {
					if (Keyboard.next()) {
						button.setKey(Keyboard.getEventKey());
					}
				}
			} else {
				// Menuebutton
				if (buttonBackToMenu.isClicked()) {
					// Ins Menue zurueckwechseln
					setNextLayer(menuLayer);
				}
			}
		}

		// Aktualisiert statische Variablen entsprechend der Buttons
		keyMoveDown = buttonMoveDown.getKey();
		keyMoveLeft = buttonMoveLeft.getKey();
		keyMoveRight = buttonMoveRight.getKey();
		keyRotateLeft = buttonRotateLeft.getKey();
		keyRotateRight = buttonRotateRight.getKey();
	}

	@Override
	public void scale() {
		// Unterteilt das Display in Reihen an denen sich die GUI Elemente orientieren
		double line = Display.getHeight() / 15d;

		// Setzt die Position und Groesse der Buttons
		for (int i = 0; i < buttons.length; i++) {
			// Setzt letzten Button tiefer
			if (i == buttons.length - 1) {
				buttons[i].setBounds(0.2 * Display.getWidth(), Display.getHeight() - (2 * i + 4) * line, 0.8 * Display
						.getWidth(), Display.getHeight() - (2 * i + 3) * line);
			} else {
				buttons[i].setBounds(0.1 * Display.getWidth(), Display.getHeight() - (2 * i + 3) * line, 0.9 * Display
						.getWidth(), Display.getHeight() - (2 * i + 2) * line);
			}
		}
	}

	@Override
	public void render() {
		// Hintergrund
		GL11.glColor3d(Main.VERY_LIGHT_GRAY_R, Main.VERY_LIGHT_GRAY_G, Main.VERY_LIGHT_GRAY_B);
		GL11.glRectd(0, 0, Display.getWidth(), Display.getHeight());

		// Buttons
		for (int i = 0; i < buttons.length; i++) {
			buttons[i].render();
		}
	}

	/**
	 * Setzt das Menue, in welches zurueckgewechselt wird.
	 *
	 * @param menuLayer Menue, in welches zurueckgewechselt wird.
	 */
	public void setMenuLayer(MenuLayer menuLayer) {
		this.menuLayer = menuLayer;
	}
}
