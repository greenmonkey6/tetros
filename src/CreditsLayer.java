import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;


/**
 * Der CreditsLayer zeigt die Credits, also die Person(en), die an dem Spiel beteiligt waren an bzw. laesst diesem
 * Schriftzug von unten nach oben ueber den Screen fahren.
 *
 * @author Tobias Schleifstein
 */
public class CreditsLayer extends Layer {
	/**
	 * Credits, die ueber den Bildschirm laufen.
	 */
	private final String[] CREDITS;

	/**
	 * Bewegungsgeschwindigkeit der Credits in pxl/ms.
	 */
	private double moveSpeed;

	/**
	 * Aktuelle Hoehe der Credits.
	 */
	private double height;

	/**
	 * Schriftgroesse der Credits.
	 */
	private double fontSize;

	/**
	 * Zeilenabstand.
	 */
	private double lineSpacing;

	/**
	 * Menue, in welches zurueckgewechselt wird.
	 */
	private MenuLayer menuLayer;

	/**
	 * Legt Konstanten fest und initialisiert den Layer.
	 */
	public CreditsLayer() {
		super();

		// Credits festlegen
		CREDITS = new String[39];
		CREDITS[0] = "PROJECT LEAD:";
		CREDITS[2] = "TOBIAS SCHLEIFSTEIN";
		CREDITS[6] = "DESIGN:";
		CREDITS[8] = "TOBIAS SCHLEIFSTEIN";
		CREDITS[12] = "PROGRAMMERS:";
		CREDITS[14] = "TOBIAS SCHLEIFSTEIN";
		CREDITS[18] = "GRAPHICS:";
		CREDITS[20] = "TOBIAS SCHLEIFSTEIN";
		CREDITS[24] = "ANIMATIONS:";
		CREDITS[26] = "TOBIAS SCHLEIFSTEIN";
		CREDITS[30] = "INTERFACE:";
		CREDITS[32] = "TOBIAS SCHLEIFSTEIN";
	}

	@Override
	public void init() {
		// Bewegungsgeschwindigkeit festlegen
		moveSpeed = 0.1;

		// Hoehe auf den unteren Bilschirmrand legen
		height = 0;

		// Schriftgroesse berechnen
		fontSize = Display.getHeight() * 0.02916; // Entspricht bei 720 ca. 21

		// Zeilenabstand setzen
		lineSpacing = 1.5;

		// Tastaturspeicher leeren
		while (Keyboard.next()) {
			// nichts
		}
	}

	@Override
	public void update(long delta) {
		// Wenn keine Taste gedrueckt ist und die Credits noch nicht durchgelaufen sind
		if (Keyboard.getNumKeyboardEvents() == 0 && height < Display.getHeight() + (CREDITS.length + 1) * fontSize *
				lineSpacing) {
			height += moveSpeed * delta;
		} else {
			// Layer neu initialisieren
			init();

			// Ins Menue zurueckwechseln
			setNextLayer(menuLayer);
		}
	}

	@Override
	public void scale() {
		// nichts
	}

	@Override
	public void render() {
		// Hintergrund
		GL11.glColor3d(Main.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B);
		GL11.glRectd(0, 0, Display.getWidth(), Display.getHeight());

		// Strings zeichnen
		for (int i = 0; i < CREDITS.length; i++) {
			if (CREDITS[i] != null) {
				Font.renderString(CREDITS[i], (Display.getWidth() - Font.getRenderedStringLength(CREDITS[i], fontSize)
				) * 0.5, height - (i + 1) * lineSpacing * fontSize, fontSize, Main.VERY_LIGHT_GRAY_R, Main
						.VERY_LIGHT_GRAY_G, Main.VERY_LIGHT_GRAY_B, 1);
			}
		}
	}

	/**
	 * Setzt das Menue, in welches zurueckgewechselt wird.
	 *
	 * @param menuLayer Menue, in welches zurueckgewechselt wird.
	 */
	public void setMenuLayer(MenuLayer menuLayer) {
		this.menuLayer = menuLayer;
	}
}
