import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;


/**
 * Die Font-Klasse ist zum Darstellen von Buchstaben bzw. Strings auf dem Bildschirm zustaendig. Sie bietet Methoden
 * einen String an einer bestimmten Position in einer bestimmten Groesse mit einer bestimmten Farbe zu rendern, sowie
 * dessen Laenge zurueckzugeben.
 * Die Buchstaben werden als Bitmatrix repraesentiert und entsprechend gerendert.
 *
 * @author Tobias Schleifstein
 */
public class Font {
	/**
	 * Array, aus String-Array, welchen einen Buchstaben definieren.
	 */
	private static String[][] letters;

	/**
	 * Buchstabenhoehe in Kacheln.
	 */
	private static final int LETTER_HEIGHT;

	static {
		LETTER_HEIGHT = 7;

		letters = new String[Keyboard.KEYBOARD_SIZE][LETTER_HEIGHT];

		// Buchstaben festlegen
		letters[' '] = new String[]{"  ","  ", "  ", "  ", "  ", "  ", "  "};
		letters['A'] = new String[]{"0000", "0  0", "0  0", "0000", "0  0", "0  0", "0  0"};
		letters['B'] = new String[]{"000 ", "0  0", "0  0", "000 ", "0  0", "0  0", "000 "};
		letters['C'] = new String[]{"0000", "0  0", "0   ", "0   ", "0   ", "0  0", "0000"};
		letters['D'] = new String[]{"000 ", "0  0", "0  0", "0  0", "0  0", "0  0", "000"};
		letters['E'] = new String[]{"0000", "0  0", "0   ", "00  ", "0   ", "0  0", "0000"};
		letters['F'] = new String[]{"0000", "0   ", "0   ", "000 ", "0   ", "0   ", "0   "};
		letters['G'] = new String[]{"0000", "0  0", "0   ", "0 00", "0  0", "0  0", "0000"};
		letters['H'] = new String[]{"0  0", "0  0", "0  0", "0000", "0  0", "0  0", "0  0"};
		letters['I'] = new String[]{"000", " 0 ", " 0 ", " 0 ", " 0 ", " 0 ", "000"};
		letters['J'] = new String[]{"0000", "0  0", "   0", "   0", "   0", "0  0", " 00 "};
		letters['K'] = new String[]{"0  0", "0  0", "0 0 ", "00 ", "0 0 ", "0  0", "0  0"};
		letters['L'] = new String[]{"0   ", "0   ", "0   ", "0   ", "0   ", "0  0", "0000"};
		letters['M'] = new String[]{"0   0", "00 00", "00 00", "0 0 0", "0   0", "0   0", "0   0"};
		letters['N'] = new String[]{"0   0", "00  0", "00  0", "0 0 0", "0  00", "0  00", "0   0"};
		letters['O'] = new String[]{"0000", "0  0", "0  0", "0  0", "0  0", "0  0", "0000"};
		letters['P'] = new String[]{"0000", "0  0", "0  0", "0000", "0   ", "0   ", "0   "};
		letters['Q'] = new String[]{"0000", "0  0", "0  0", "0  0", "0  0", "0 00", "0000"};
		letters['R'] = new String[]{"0000", "0  0", "0  0", "0000", "0 0 ", "0  0", "0  0"};
		letters['S'] = new String[]{"0000", "0  0", "0   ", "0000", "   0", "0  0", "0000"};
		letters['T'] = new String[]{"00000", "0 0 0", "  0  ", "  0  ", "  0  ", "  0  ", " 000 "};
		letters['U'] = new String[]{"0  0", "0  0", "0  0", "0  0", "0  0", "0  0", "0000"};
		letters['V'] = new String[]{"0   0", "0   0", "0   0", "0   0", " 0 0 ", " 0 0 ", "  0  "};
		letters['W'] = new String[]{"0   0", "0   0", "0   0", "0 0 0", "00 00", "00 00", "0   0"};
		letters['X'] = new String[]{"0   0", "0   0", " 0 0 ", "  0  ", " 0 0 ", "0   0", "0   0"};
		letters['Y'] = new String[]{"0   0", "0   0", "0   0", "00000", "  0  ", "  0  ", "  0  "};
		letters['Z'] = new String[]{"00000", "    0", "   0 ", "  0  ", " 0   ", "0    ", "00000"};
		letters['0'] = new String[]{"0000", "0  0", "0  0", "0  0", "0  0", "0  0", "0000"};
		letters['1'] = new String[]{"  0 ", " 00 ", "  0 ", "  0 ", "  0 ", "  0 ", "  0 "};
		letters['2'] = new String[]{"0000", "   0", "   0", "0000", "0   ", "0   ", "0000"};
		letters['3'] = new String[]{"0000", "   0", "   0", " 000", "   0", "   0", "0000"};
		letters['4'] = new String[]{"0  0", "0  0", "0  0", "0000", "   0", "   0", "   0"};
		letters['5'] = new String[]{"0000", "0   ", "0   ", "0000", "   0", "   0", "0000"};
		letters['6'] = new String[]{"0000", "0   ", "0   ", "0000", "0  0", "0  0", "0000"};
		letters['7'] = new String[]{"0000", "   0", "   0", "   0", "   0", "   0", "   0"};
		letters['8'] = new String[]{"0000", "0  0", "0  0", "0000", "0  0", "0  0", "0000"};
		letters['9'] = new String[]{"0000", "0  0", "0  0", "0000", "   0", "   0", "0000"};
		letters['\u00C4'] = new String[]{"0  0", "    ", "0000", "0  0", "0000", "0  0", "0  0"};
		letters['\u00D6'] = new String[]{"0  0", "    ", "0000", "0  0", "0  0", "0  0", "0000"};
		letters['\u00DC'] = new String[]{"0  0", "    ", "0  0", "0  0", "0  0", "0  0", "0000"};
		letters['!'] = new String[]{"0", "0", "0", "0", "0", " ", "0"};
		letters['-'] = new String[]{"   ", "   ", "   ", "000", "   ", "   ", "   "};
		letters['.'] = new String[]{" ", " ", " ", " ", " ", " ", "0"};
		letters['/'] = new String[]{"  0", "  0", " 0 ", " 0 ", " 0 ", "0  ", "0  "};
		letters[':'] = new String[]{" ", " ", "0", " ", " ", "0", " "};
		letters['|'] = new String[]{"0", "0", "0", "0", "0", "0", "0"};
	}

	/**
	 * Rendert einen String an einer bestimmten Stelle in einer bestimmten Farbe an einer bestimmten Stelle auf dem
	 * Display.
	 *
	 * @param str   Zu rendernder String.
	 * @param x     x-Koordinate der Position, an der der String angezeigt werden
	 *              soll.
	 * @param y     y-Koordinate der Position, an der der String angezeigt werden
	 *              soll.
	 * @param size  Groesse, in welcher der String angezeigt werden soll.
	 * @param r     Roter Farbanteil der Farbe, in welcher der String dargestellt
	 *              werden soll.
	 * @param g     Gruener Farbanteil der Farbe, in welcher der String dargestellt
	 *              werden soll.
	 * @param b     Blauer Farbanteil der Farbe, in welcher der String dargestellt
	 *              werden soll.
	 * @param alpha Transparanz der Farbe, in welcher der String dargestellt
	 *              werden soll.
	 */
	public static void renderString(String str, double x, double y, double size, double r, double g, double b, double
			alpha) {
		// Setzt Farbe der Schrift
		GL11.glColor4d(r, g, b, alpha);

		// Groesse einer Kachel
		double width = size / LETTER_HEIGHT;

		// Cursor zeigt auf aktuelle Position auf dem Display beim Durchgehen der Buchstaben des Strings
		double cursor = 0;

		// Geht Buchstaben des Strings durch
		for (int i = 0; i < str.length(); i++) {
			// Wenn der Buchstabe in Font existiert
			if (letters[str.charAt(i)][0] != null) {
				// Geht den Buchstaben durch
				for (int j = 0; j < letters[str.charAt(i)].length; j++) {
					for (int k = 0; k < letters[str.charAt(i)][j].length(); k++) {
						if (letters[str.charAt(i)][j].charAt(k) == '0') {
							// Zeichnet die Kachel
							GL11.glRectd(cursor + x + k * width, y + (LETTER_HEIGHT - j) * width, cursor + x +
									(k + 1) * width, y + LETTER_HEIGHT * width - (j + 1) * width);
						}
					}
				}

				// Bewegt den Cursor weiter
				cursor += (letters[str.charAt(i)][0].length() + 1) * width;
			}
		}
	}

	/**
	 * Gibt die Laenge in pxl zurueck, die der uebergebene String haette, wenn er auf dem Bildschirm gerendert werden
	 * wuerde.
	 *
	 * @param str  Gerendeter String.
	 * @param size Groesse des gerendeten Strings.
	 * @return Laenge des gerenderten Strings.
	 */
	public static double getRenderedStringLength(String str, double size) {
		double length = 0;

		// Addiert Laengen aller Buchstaben
		for (int i = 0; i < str.length(); i++) {
			if (letters[str.charAt(i)][0] != null) {
				length += (letters[str.charAt(i)][0].length() + 1) * size / LETTER_HEIGHT;
			}
		}

		length -= size / LETTER_HEIGHT;

		return length;
	}

	/**
	 * Gibt zurueck, ob ein bestimmter Buchstabe unterstuetzt wird.
	 *
	 * @param letter Zu pruefender Buchstabe.
	 * @return Gibt an, ob ein der Buchstabe unterstuetzt wird.
	 */
	public static boolean isSupported(char letter) {
		return letters[letter][0] != null;
	}
}
