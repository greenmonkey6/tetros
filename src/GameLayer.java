import java.util.Random;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;


/**
 * Der GameLayer verwaltet das eigentliche Spiel. Ein zweidimensionales Integer-Array stellt das Spielfeld dar. In
 * diesem spawnen dann die Tetrominos und fallen nach unten, wobei der Spieler diese im Fall steuern kann. Wenn eine
 * Reihe komplett voll ist, verschwindet diese. Ausserdem berechnet der GameLayer die entsprechenden Punkte, das
 * Level und die bereits zerstoerten Reihen. Das Spiel ist verloren, wenn die kein neues Tetromino mehr gespawnt
 * werden kann. Dann wird der Punktestand an den HighscoresLayer uebergeben.
 *
 * @author Tobias Schleifstein
 */
public class GameLayer extends Layer {
	/**
	 * Groesse des Spielfeldes in Tetrinobreiten.
	 */
	private static final int FIELDSIZE_X, FIELDSIZE_Y;

	/**
	 * Groesse eines Tetrominos in Tetrinobreiten.
	 */
	private static final int TETROMINO_SIZE;

	/**
	 * Richtungen, in welche sich die Tetrominos bewegen koennen.
	 */
	private static final int LEFT, DOWN, RIGHT;

	/**
	 * Fallgeschwindigkeit des Tetrominos, wenn es schneller faellt in Hz.
	 */
	private static final double FALLSPEED_AT_FASTDOWN;

	/**
	 * Zustaende, die die Kacheln des Spielfeldes annehmen koennen
	 */
	private static final int NONE, MOVEABLE, LOCKED, MARKED;

	static {
		// Spielfeldgroesse einstellen
		FIELDSIZE_X = 10;
		FIELDSIZE_Y = 18;

		// Tetrominobreite festlegen
		TETROMINO_SIZE = 4;

		// Richtungen festlegen
		LEFT = 0;
		DOWN = 1;
		RIGHT = 2;

		// Fallgeschwindigkeit beim schnellen Fallen festlegen
		FALLSPEED_AT_FASTDOWN = 15;

		// Zustaende des Spielfeldes setzen
		NONE = 0;
		MOVEABLE = 1;
		LOCKED = 2;
		MARKED = 3;
	}

	/**
	 * Spielfeld, in Form eines zweidimaensionalen Integer-Gitters.
	 */
	private int[][] field;

	/**
	 * Alle zweidimensionale Tetromino-Vorlagen in Ausgansposition.
	 */
	private int[][][] tetrominos;

	/**
	 * Koordinaten des aktuellen Tetrominos, was im Spiel ist und bewegt werden kann (bzw. des 4x4 Blocks um das
	 * Tetrimino).
	 */
	private int currentTetrominoX, currentTetrominoY;

	/**
	 * Die Fallgeschwindigkeit der Tetrominos in Hz.
	 */
	private double fallSpeed;

	/**
	 * Speichert, wie viel Zeit in ms seit dem letzten Fallen bereits vergangen ist, um passend zur
	 * Fallgeschwindigkeit zu agieren.
	 */
	private long fallTime;

	/**
	 * Dieses Random-Objekt ist fuer die zufaellige Auswahl an Terominos zustaendig.
	 */
	private Random random;

	/**
	 * Gibt an, ob sich die Tetrominos schneller nach unten bewegen, also ob der User dies fordert.
	 */
	private boolean fastDown;

	/**
	 * Punktestand des Spielers.
	 */
	private int score;

	/**
	 * Level, in welchem sich der Spieler befindet.
	 */
	private int level;

	/**
	 * Anzahl der Reihen, die der Spieler bisher aufgeloest hat.
	 */
	private int lines;

	/**
	 * Tetromino, welches als naechstes gespawnt wird.
	 */
	private int nextTetromino;

	/**
	 * Gibt an, ob das Spiel laeuft.
	 */
	private boolean running;

	/**
	 * 'Pause'-Button, um das Spiel zu pausieren.
	 */
	private Button buttonPause;

	/**
	 * 'Quit'-Button, um das Spiel zu beenden
	 */
	private Button buttonQuit;

	/**
	 * Array mit allen Buttons.
	 */
	private Button[] buttons;

	/**
	 * Highscore-Layer, in welchen das Spiel nach Beendigung wechselt.
	 */
	private HighscoresLayer highscoresLayer;

	@Override
	public void init() {
		// Spielfeld initialisieren
		field = new int[FIELDSIZE_Y][FIELDSIZE_X];

		// Tetrominos definieren
		tetrominos = new int[7][4][4];
		// I
		tetrominos[0] = new int[][]{{0, 0, 0, 0}, {1, 1, 1, 1}, {0, 0, 0, 0}, {0, 0, 0, 0}};
		// J
		tetrominos[1] = new int[][]{{0, 0, 0, 0}, {1, 1, 1, 0}, {0, 0, 1, 0}, {0, 0, 0, 0}};
		// L
		tetrominos[2] = new int[][]{{0, 0, 0, 0}, {1, 1, 1, 0}, {1, 0, 0, 0}, {0, 0, 0, 0}};
		// O
		tetrominos[3] = new int[][]{{0, 0, 0, 0}, {0, 1, 1, 0}, {0, 1, 1, 0}, {0, 0, 0, 0}};
		// S
		tetrominos[4] = new int[][]{{0, 0, 0, 0}, {0, 1, 1, 0}, {1, 1, 0, 0}, {0, 0, 0, 0}};
		// T
		tetrominos[5] = new int[][]{{0, 0, 0, 0}, {1, 1, 1, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}};
		// Z
		tetrominos[6] = new int[][]{{0, 0, 0, 0}, {1, 1, 0, 0}, {0, 1, 1, 0}, {0, 0, 0, 0}};

		// Fallzeit auf 0 setzen
		fallTime = 0;

		// Random-Objekt initialisieren
		random = new Random();

		// Festlegen, dass die Steine nicht schnell fallen
		fastDown = false;

		// Spieleigenschaften initialisieren
		score = 0;
		lines = 0;
		calcLevelAndSpeed();

		// Tetromino spawnen und naechstes festelegen
		spawnTetromino(random.nextInt(tetrominos.length));
		nextTetromino = random.nextInt(tetrominos.length);

		// Spiel nicht pausieren
		running = true;

		// Buttons initialisieren
		buttonPause = new Button();
		buttonQuit = new Button();
		buttonQuit.setTitle("QUIT");

		buttons = new Button[2];
		buttons[0] = buttonPause;
		buttons[1] = buttonQuit;

		// Alles richtig skalieren
		scale();

		// Tastaturspeicher leeren
		while (Keyboard.next()) {
			// nichts
		}
	}

	@Override
	public void update(long delta) {
		// Auf Tasteneingaben reagieren
		while (Keyboard.next()) {
			// Waehrend das Spiel laeuft
			if (running) {
				// Wenn eine Taste gedrueckt wird
				if (Keyboard.getEventKeyState()) {
					if (Keyboard.getEventKey() == ControlsLayer.keyMoveLeft) {
						move(LEFT);
					} else if (Keyboard.getEventKey() == ControlsLayer.keyMoveRight) {
						move(RIGHT);
					} else if (Keyboard.getEventKey() == ControlsLayer.keyRotateLeft) {
						rotate(false); // gegen den Uhrzeigersinn (links)
					} else if (Keyboard.getEventKey() == ControlsLayer.keyRotateRight) {
						rotate(true); // im Uhrzeigersinn (rechts)
					}
				}

				if (Keyboard.getEventKey() == ControlsLayer.keyMoveDown) {
					fastDown = Keyboard.getEventKeyState();
				}
			}
		}

		// Buttons.updaten
		for (int i = 0; i < buttons.length; i++) {
			buttons[i].update();
		}

		// Auf Buttons reagieren
		if (buttonPause.isClicked()) {
			running = !running;
		}

		if (buttonQuit.isClicked()) {
			gameOver();
		}

		if (running) {
			// Fallzeit weiterzaehlen
			fallTime += delta;

			// Wenn die Fallzeit gross genug ist, um das Tetromino weiter fallen
			// zu lassen
			if (fallTime >= 1000 / fallSpeed && !fastDown || fallTime >= 1000 / FALLSPEED_AT_FASTDOWN && fastDown) {
				// Laesst das Tetromino fallen, setzt die Fallzeit zurueck und speichert, ob sich noch Tetrinos im
				// Spielfeld bewegen koennen
				boolean moving = move(DOWN);
				fallTime = 0;

				// Zerstoert volle Reihen
				destroyFullLines();

				// Berechnet Level und Geschwindigkeit passend
				calcLevelAndSpeed();

				// Spawnt ein neues Tetromino, wenn sich kein Tetrino mehr bewegen kann und spawnt das Tetromino nur,
				// wenn es keine markierten Reihen gibt
				if (!moving && !hasMarkedLines()) {
					spawnTetromino(nextTetromino);
					nextTetromino = random.nextInt(tetrominos.length);
				}
			}
		}
	}

	public void scale() {
		// Buttons
		if (running) {
			buttonPause.setTitle("PAUSE");
		} else {
			buttonPause.setTitle("RESUME");
		}
	}

	@Override
	public void render() {
		// Hintergrund
		GL11.glColor3d(Main.VERY_LIGHT_GRAY_R, Main.VERY_LIGHT_GRAY_G, Main.VERY_LIGHT_GRAY_B);
		GL11.glRectd(0, 0, Display.getWidth(), Display.getHeight());

		// Breite eines Tetrinos auf dem Bildschirm
		double width = Math.min(1d * Display.getHeight() / FIELDSIZE_Y, 1d * Display.getWidth() / FIELDSIZE_X);

		// Mitte des Bildschirms
		double mid = (Display.getWidth() - (FIELDSIZE_X + 1 + TETROMINO_SIZE) * width) * 0.5;

		// Geht das Spielfeld durch
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				// Setzt die Farbe entsprechend des Tetrinos
				if (field[i][j] == NONE) {
					GL11.glColor3d(Main.LIGHT_GRAY_R, Main.LIGHT_GRAY_G, Main.LIGHT_GRAY_B);
				} else if (field[i][j] == MOVEABLE) {
					GL11.glColor3d(Main.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B);
				} else if (field[i][j] == LOCKED) {
					GL11.glColor3d(Main.GRAY_R, Main.GRAY_G, Main.GRAY_B);
				} else if (field[i][j] == MARKED) {
					GL11.glColor3d(Main.WHITE_R, Main.WHITE_G, Main.WHITE_B);
				}

				// Zeichnet das Tetrino
				GL11.glRectd(mid + j * width, Display.getHeight() - (i + 1) * width, mid + (j + 1) * width - 1,
						Display.getHeight() - i * width - 1);
			}
		}

		// Schrift einstellen
		double fontSize = width * 0.875;

		// Score
		Font.renderString("SCORE", mid + (FIELDSIZE_X + 1) * width, Display.getHeight() - 2 * width, fontSize, Main
				.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B, 1);
		Font.renderString(score + "", mid + (FIELDSIZE_X + 1) * width, Display.getHeight() - 3 * width, fontSize, Main
				.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B, 1);

		// Level
		Font.renderString("LEVEL", mid + (FIELDSIZE_X + 1) * width, Display.getHeight() - 5 * width, fontSize, Main
				.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B, 1);
		Font.renderString(level + "", mid + (FIELDSIZE_X + 1) * width, Display.getHeight() - 6 * width, fontSize, Main
				.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B, 1);

		// Lines
		Font.renderString("LINES", mid + (FIELDSIZE_X + 1) * width, Display.getHeight() - 8 * width, fontSize, Main
				.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B, 1);
		Font.renderString(lines + "", mid + (FIELDSIZE_X + 1) * width, Display.getHeight() - 9 * width, fontSize, Main
				.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B, 1);

		// Zeichnet das Tetrino, welches als naechstes kommt

		int[][] tetromino = tetrominos[nextTetromino];

		// Geht das Tetrino durch
		for (int i = 0; i < tetromino.length; i++) {
			for (int j = 0; j < tetromino[i].length; j++) {
				// Setzt die Farbe entsprechend des Tetrinos
				if (tetromino[i][j] == NONE) {
					GL11.glColor3d(Main.LIGHT_GRAY_R, Main.LIGHT_GRAY_G, Main.LIGHT_GRAY_B);
				} else if (tetromino[i][j] == MOVEABLE) {
					GL11.glColor3d(Main.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B);
				} else if (tetromino[i][j] == LOCKED) {
					GL11.glColor3d(Main.GRAY_R, Main.GRAY_G, Main.GRAY_B);
				} else if (tetromino[i][j] == MARKED) {
					GL11.glColor3d(Main.WHITE_R, Main.WHITE_G, Main.WHITE_B);
				}

				// Zeichnet das Tetrino
				GL11.glRectd(mid + (FIELDSIZE_X + 1) * width + j * width, Display.getHeight() - 10 * width - (i + 1) *
						width, mid + (FIELDSIZE_X + 1) * width + (j + 1) * width - 1, Display.getHeight() - 10 * width
						- i * width - 1);
			}
		}

		// Buttons
		for (int i = 0; i < buttons.length; i++)

		{
			buttons[i].setBounds(mid + (FIELDSIZE_X + 1) * width, Display.getHeight() - (i + 16) * width, mid +
					(FIELDSIZE_X + 1 + TETROMINO_SIZE) * width, Display.getHeight() - (i + 15) * width);
			buttons[i].render();
		}

	}

	/**
	 * Spawnt ein Tetromino in der obeneren Mitte des Spieldfeldes. Ist dies nicht moeglich, ist das Spiel verloren.
	 *
	 * @param tetromino Teromino, welches gespawnt werde soll (I, J, L, O, S, T oder Z)
	 */
	private void spawnTetromino(int tetromino) {
		// Um gewisse Fehler zu vermeiden
		boolean gameOver = false;

		// Geht das Tetromino durch
		for (int i = 0; i < tetrominos[tetromino].length; i++) {
			for (int j = 0; j < tetrominos[tetromino][i].length; j++) {
				// Obere linke Ecke des Tetrominos auf dem Spielfeld
				currentTetrominoX = (field[i].length - tetrominos[tetromino][i].length) / 2;
				currentTetrominoY = 0;

				// Schreibt das Tetrino in die Entsprechende Stelle des Feldes, solange dies frei ist, wenn nicht, ist
				// das Spiel zuende
				if (field[currentTetrominoY + i][currentTetrominoX + j] == NONE) {
					field[currentTetrominoY + i][currentTetrominoX + j] = tetrominos[tetromino][i][j];
				} else if (tetrominos[tetromino][i][j] != 0) {
					// Spiel beenden
					gameOver = true;
				}
			}
		}

		if (gameOver) {
			gameOver();
		}
	}

	/**
	 * Bewegt alle beweglichen Tetrinos und gibt zurueck, ob es bewegliche Tetrinos gibt.
	 *
	 * @param direction Richtung, in welche sich die Tetrinos bewegen sollen. Wird durch die Konstanten LEFT, DOWN und
	 *                  RIGHT definiert.
	 * @return Gibt an, ob es bewegliche Tetrinos gibt.
	 */
	private boolean move(int direction) {
		// Ueberprueft, ob die Richtung zugelassen ist
		if (direction == LEFT || direction == DOWN || direction == RIGHT) {
			// Ueberprueft, ob das aktuelle Tetrino noch bewegbar ist
			boolean moving = false;

			// Ueberprueft, ob sich alle beweglichen Tetrinos in die entsprechende Richtung bewegen koennen
			boolean moveable = true;

			// Geht das Spielfeld durch
			for (int i = 0; i < field.length; i++) {
				for (int j = 0; j < field[i].length; j++) {
					// Wenn das Tetrino beweglich ist
					if (field[i][j] == MOVEABLE) {
						// Gibt an, dass es bewegliche Tetrinos gibt
						moving |= true;

						// Wenn das Tetrino kein freies Feld oder sich selbst in entsprechender Richtung hat
						if ((direction == LEFT && (j <= 0 || (field[i][j - 1] != NONE && field[i][j - 1] != MOVEABLE)))
								|| (direction == DOWN && (i >= field.length - 1
									|| (field[i + 1][j] != NONE && field[i + 1][j] != MOVEABLE)))
								|| (direction == RIGHT && (j >= field[i].length - 1
									|| (field[i][j + 1] != NONE && field[i][j + 1] != MOVEABLE)))) {
							// Setzt des Status auf 'nicht bewegbar' und verlaesst die Schleife
							moveable = false;
							i = field.length - 1;
							j = field[i].length;
						}
					}
				}
			}

			// Bewegt die Tetrinos

			// Geht das Spielfeld von unten nach oben durch und von links nach rechts bzw. rechnet fuer die Bewegung 
			// des Tetrinos nach rechts das Durchgehen so um, als ginge es von rechts nach links
			for (int i = field.length - 1; i >= 0; i--) {
				for (int j = 0; j < field[i].length; j++) {
					// Wenn das Tetrino beweglich ist
					if ((direction != RIGHT && field[i][j] == MOVEABLE) || (direction == RIGHT && field[i][field[i]
							.length -
							1 - j] == MOVEABLE)) {
						// Wenn sich Tetrinos bewegen koennen
						if (moveable) {
							// Kopiert das Tetrino in die entsprechende Richtung
							if (direction == LEFT) {
								field[i][j - 1] = field[i][j];
								field[i][j] = NONE;
							} else if (direction == DOWN) {
								field[i + 1][j] = field[i][j];
								field[i][j] = NONE;
							} else if (direction == RIGHT) {
								field[i][field[i].length - j] = field[i][field[i].length - 1 - j];
								field[i][field[i].length - 1 - j] = NONE;
							}
						} else {
							// Wenn sich die Tetrinos nach unten bewegen, werden sie fixiert
							if (direction == DOWN) {
								field[i][j] = LOCKED;
							}
						}
					}
				}
			}

			// Setzt die Variablen fuer die Position des aktuellen Tetrominos entsprechend
			if (moveable) {
				if (direction == LEFT) {
					currentTetrominoX--;
				} else if (direction == DOWN) {
					currentTetrominoY++;

					// Erhoeht pro Reihe, welche schnell durchfallen wurde den Punktestand
					if (fastDown) {
						score++;
					}
				} else if (direction == RIGHT) {
					currentTetrominoX++;
				}
			}

			return moving;
		} else {
			System.err.println("Richtung nicht vorhanden: " + direction);
			return true;
		}
	}

	/**
	 * Dreht das aktuelle Tetromino.
	 *
	 * @param direction Richtung der Rotation: true = im Uhrzeigersinn, false = gegen den Uhrzeigersinn.
	 */
	private void rotate(boolean direction) {
		// Liest das aktuelle Tetromino aus dem Spielfeld aus
		int[][] tetromino = new int[TETROMINO_SIZE][TETROMINO_SIZE];

		// Geht das Tetromino im Spielfeld durch und uebernimmt die Werte
		for (int i = 0; i < TETROMINO_SIZE; i++) {
			for (int j = 0; j < TETROMINO_SIZE; j++) {
				// Wenn sich das Tetrino ausserhalb des Spielfeldes befindet, wird es als fixiert gewertet
				if (currentTetrominoY + i < 0 || currentTetrominoY + i > field.length - 1 || currentTetrominoX + j < 0
						|| currentTetrominoX + j > field[i].length - 1) {
					tetromino[i][j] = LOCKED;
				} else {
					tetromino[i][j] = field[currentTetrominoY + i][currentTetrominoX + j];
				}

			}
		}

		// Dreht das Tetromino

		int[][] rotatedTetromino = new int[TETROMINO_SIZE][TETROMINO_SIZE];
		boolean rotateable = true;

		// Geht das Tetromino durch
		for (int i = 0; i < tetromino.length; i++) {
			for (int j = 0; j < tetromino[i].length; j++) {
				// Rotiert alle beweglichen Tetrinos
				if (tetromino[i][j] == MOVEABLE) {
					int i2, j2;

					if (direction == true) {
						i2 = j;
						j2 = TETROMINO_SIZE - 1 - i;
					} else {
						i2 = TETROMINO_SIZE - 1 - j;
						j2 = i;
					}

					// Rotiert nur, wenn das entsprechende Feld frei ist
					if (tetromino[i2][j2] == NONE || tetromino[i2][j2] == MOVEABLE) {
						rotatedTetromino[i2][j2] = tetromino[i][j];
					} else {
						rotateable = false;
					}
				}
				// Uebernimmt alle restlichen Werte
				else if (tetromino[i][j] != NONE) {
					rotatedTetromino[i][j] = tetromino[i][j];
				}
			}
		}

		// Schreibt das Tetromino zurueck ins Feld, wenn das Rotieren erfolgreich war
		if (rotateable) {
			for (int i = 0; i < rotatedTetromino.length; i++) {
				for (int j = 0; j < rotatedTetromino[i].length; j++) {
					if (!(currentTetrominoY + i < 0 || currentTetrominoY + i > field.length - 1 || currentTetrominoX +
							j < 0 || currentTetrominoX + j > field[i].length - 1)) {
						field[currentTetrominoY + i][currentTetrominoX + j] = rotatedTetromino[i][j];
					}
				}
			}
		}
	}

	/**
	 * Zerstoert alle vollen Reihen und rueckt die Tetrinos passend ein.
	 */
	private void destroyFullLines() {
		// Speichert Anzahl der zerstoerten Reihen zwischen
		int linesBefore = lines;

		// Zerstoert markierte Reihen
		for (int i = field.length - 1; i >= 0; i--) {
			// Wenn die Reihe markiert ist
			if (field[i][0] == MARKED) {
				// Rueckt Reihen nach
				for (int j = i - 1; j >= 0; j--) {
					field[j + 1] = field[j];
					field[j] = new int[FIELDSIZE_X];
				}

				// Zaehlt die Anzahl der bisher zerstoerten Reihen hoch
				lines++;

				// Bleibt in der aktuellen Reihe, da Reihen nachgerueckt sind
				i++;
			}
		}

		// Punkte fuer zerstoerte Reihen erhoehen
		switch (lines - linesBefore) {
			case 1:
				score += 40 * (level + 1);
				break;
			case 2:
				score += 100 * (level + 1);
				break;
			case 3:
				score += 300 * (level + 1);
				break;
			case 4:
				score += 1200 * (level + 1);
				break;
		}


		// Geht alle Reihen durch
		for (int i = 0; i < field.length; i++) {
			// Ueberprueft, ob die Reihe voll ist
			boolean fullLine = true;

			for (int j = 0; j < field[i].length; j++) {
				if (field[i][j] == NONE || field[i][j] == MOVEABLE) {
					fullLine = false;
				}
			}

			// Markiert die Reihe, wenn sie voll ist
			if (fullLine) {
				for (int j = 0; j < field[i].length; j++) {
					field[i][j] = MARKED;
				}
			}
		}
	}

	private boolean hasMarkedLines() {
		boolean markedLines = false;

		// Geht alle Tetrinos durch und Ueberprueft, ob sie markiert sind
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				if (field[i][j] == MARKED) {
					markedLines = true;
				}
			}
		}

		return markedLines;
	}

	/**
	 * Berechnet das aktuelle Level und die aktuelle Fallgeschwindigkeit.
	 */
	private void calcLevelAndSpeed() {
		level = lines / 10;
		fallSpeed = 1 / (1 - (Math.min(level, 10) + 2) * 0.07);
	}

	/**
	 * Beendet das Spiel und kehrt zum Menue zurueck
	 */
	private void gameOver() {
		// Neuen Highscore erstellen
		highscoresLayer.addHighScore(score);

		// Zu den Highscores wechseln
		setNextLayer(highscoresLayer);

		// Spiel neu initialisieren
		init();
	}

	/**
	 * Setzt den Highscore-Layer, in welchen das Spiel nach Beendigung wechselt.
	 *
	 * @param highscoresLayer Highscore-Layer, in welchen das Spiel nach Beendigung
	 *                        wechselt.
	 */
	public void setHighscoresLayer(HighscoresLayer highscoresLayer) {
		this.highscoresLayer = highscoresLayer;
	}
}
