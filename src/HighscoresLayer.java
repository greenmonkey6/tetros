import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;


/**
 * Der HighscoresLayer ist der Layer, welcher die Hoechstpunktzahlen des Spielers mit dessen Namen anzeigt bzw. in
 * welchem man diesen Namen waehlen kann.
 *
 * @author Tobias Schleifstein
 */
public class HighscoresLayer extends Layer {
	/**
	 * Markiert einen Highscore, um dessen Namen zu aendern.
	 */
	public final static String MARKED;

	static {
		MARKED = "marked";
	}

	/**
	 * Liste mit den Namen der Highscores.
	 */
	public static String[] highscoresName;

	/**
	 * Liste mit den Werten der Highscores.
	 */
	public static int[] highscoresValue;

	/**
	 * Tastatureingabe, welche angezeigt wird, bis Enter gedrueckt wurde.
	 */
	private String input;

	/**
	 * Button, um ins Menue zurueckzukehren
	 */
	private Button buttonBackToMenu;

	/**
	 * Zeit, die seit dem letzten Blinken zurueckliegt.
	 */
	private long cursorDuration;

	/**
	 * Zeit, die der Cursor zum Blinken braucht.
	 */
	private long cursorTime;

	/**
	 * Breite einer Zeile.
	 */
	private double line;

	/**
	 * Menue in welches das Spiel zurueckwechselt.
	 */
	private MenuLayer menuLayer;

	@Override
	public void init() {
		// Input leeren
		input = "";

		// Button initialisieren
		buttonBackToMenu = new Button();
		buttonBackToMenu.setTitle("BACK TO MENU");

		// Blinkzeit des Cursors festlegen
		cursorTime = 1060;
		cursorDuration = 0;

		// Alles richtig skalieren
		scale();
	}

	@Override
	public void update(long delta) {
		// Button updaten
		buttonBackToMenu.update();

		// Auf Button reagieren
		if (buttonBackToMenu.isClicked()) {
			// Zurueck ins Menue wechseln
			setNextLayer(menuLayer);
		}

		// Auf Tastatur reagieren
		while (Keyboard.next()) {
			if (Keyboard.getEventKeyState() == true) {
				// Wenn Enter gedrueckt wird, Input speichern
				if (Keyboard.getEventKey() == Keyboard.KEY_RETURN) {
					// Nur wenn der String eine Laenge hat
					if (input.length() != 0) {
						// Bei allen markierten Highscores die aktuelle Eingabe als Namen sezten
						for (int i = 0; i < highscoresName.length; i++) {
							if (highscoresName[i].equals(MARKED)) {
								highscoresName[i] = input.trim();
							}
						}

						input = "";
					}
				}
				// Wenn die Zurueck-Taste gedrueckt wird Input kuerzen
				else if (Keyboard.getEventKey() == Keyboard.KEY_BACK) {
					// Nur wenn der String eine Laenge hat
					if (input.length() != 0) {
						input = input.substring(0, input.length() - 1);
					}
				} else {
					// Buchstaben gross machen
					char letter = Character.toUpperCase(Keyboard.getEventCharacter());

					// Ueberpruefen und Schreiben
					if (Font.isSupported(letter)) {
						input += letter;
					}
				}
			}
		}


		// Solange Highscores markiert sind, Button nicht drueckbar machen
		boolean hasMarked = false;
		for (int i = 0; i < highscoresName.length; i++) {
			if (highscoresName[i].equals(MARKED)) {
				hasMarked = true;
			}
		}

		buttonBackToMenu.setPressable(!hasMarked);

		// Cursorzeit erhoehen
		if (cursorDuration > cursorTime) {
			cursorDuration = 0;
		}
		cursorDuration += delta;
	}

	@Override
	public void scale() {
		// Zeilenhoehe berechnen
		line = Display.getHeight() / 15d;

		// Button skalieren
		buttonBackToMenu.setBounds(0.2 * Display.getWidth(), line, 0.8 * Display.getWidth(), 2 * line);
	}

	@Override
	public void render() {
		// Hintergrund
		GL11.glColor3d(Main.VERY_LIGHT_GRAY_R, Main.VERY_LIGHT_GRAY_G, Main.VERY_LIGHT_GRAY_B);
		GL11.glRectd(0, 0, Display.getWidth(), Display.getHeight());

		// Schriftgroesse berechnen
		double fontSize = Display.getHeight() * 0.04861; // entspricht bei 720 ca. 35

		// Highscores
		for (int i = 0; i < highscoresName.length; i++) {
			// Markierte Highscores
			if (highscoresName[i].equals(MARKED)) {
				String temp;

				// Cursor der Eingabe auf dem Bildschirm hinzufuegen
				if (cursorDuration >= cursorTime * 0.5) {
					temp = input + '|';
				} else {
					temp = input;
				}

				// Grauer Kasten um den markierten Highscore
				GL11.glColor3d(Main.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B);
				GL11.glRectd(0.2 * Display.getWidth() - 0.1 * line, Display.getHeight() - (i + 1) * line - 0.1 * line,
						0.2 * Display.getWidth() + Font.getRenderedStringLength(String.format("%d. %d - %s", i + 1,
								highscoresValue[i], temp), fontSize) + 0.1 * line, Display.getHeight() - (i + 2) *
								line - 0.1 * line);

				// Highscore
				Font.renderString(String.format("%d. %d - %s", i + 1, highscoresValue[i], temp), 0.2 * Display
						.getWidth(), Display.getHeight() - (i + 2) * line, fontSize, Main.VERY_LIGHT_GRAY_R, Main
						.VERY_LIGHT_GRAY_G, Main.VERY_LIGHT_GRAY_B, 1);
			} else {
				// Nichtmarkierte Highscores
				Font.renderString(String.format("%d. %d - %s", i + 1, highscoresValue[i], highscoresName[i]), 0.2 *
						Display.getWidth(), Display.getHeight() - (i + 2) * line, fontSize, Main.DARK_GRAY_R, Main
						.DARK_GRAY_G, Main.DARK_GRAY_B, 1);
			}
		}

		// Button
		buttonBackToMenu.render();
	}

	/**
	 * Fuegt einen Highscore hinzu.
	 *
	 * @param value Hoehe des Highscores.
	 */
	public void addHighScore(int value) {
		// Highscore einordnen
		for (int i = 0; i < highscoresValue.length; i++) {
			if (value > highscoresValue[i]) {
				// Highscores nach unten ruecken
				for (int j = highscoresValue.length - 2; j >= i; j--) {
					highscoresName[j + 1] = highscoresName[j];
					highscoresValue[j + 1] = highscoresValue[j];
				}

				// Highscore setzten und markieren, sodass der Name eingegeben werden kann
				highscoresName[i] = MARKED;
				highscoresValue[i] = value;

				// Schleife verlassen
				i = highscoresValue.length;
			}
		}
	}

	/**
	 * Setzt das Menue, in welches zurueckgewechselt wird.
	 *
	 * @param menuLayer Menue, in welches zurueckgewechselt wird.
	 */
	public void setMenuLayer(MenuLayer menuLayer) {
		this.menuLayer = menuLayer;
	}
}
