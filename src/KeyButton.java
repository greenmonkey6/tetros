import org.lwjgl.input.Keyboard;


/**
 * Ein KeyButton ist ein Button, welcher darauf ausgelegt ist die Tastenbelegung fuer eine bestimmte Aktion im Spiel
 * darzustellen.
 *
 * @author Tobias Schleifstein
 */
public class KeyButton extends Button {
	/**
	 * Taste, welche fuer die Aktion zu druecken ist.
	 */
	private int key;

	/**
	 * Aktion, welche durch den Button ausgeloest werden soll.
	 */
	private String action;

	@Override
	public void setTitle(String title) {
		action = title;

		super.setTitle(action + ": " + Keyboard.getKeyName(key));
	}

	/**
	 * Setzt die Taste, welche fuer die Aktion zu druecken ist.
	 *
	 * @param key Taste, welche fuer die Aktion zu druecken ist.
	 */
	public void setKey(int key) {
		this.key = key;

		setTitle(action);
	}

	/**
	 * Gibt die Taste zurueck, welche fuer die Aktion zu druecken ist.
	 *
	 * @return Taste, welche fuer die Aktion zu druecken ist.
	 */
	public int getKey() {
		return key;
	}
}
