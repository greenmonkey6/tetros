/**
 * Ein Layer ist ein abstraktes Konstrukt, um auf dem Bildschirm mehrere unabhaengige Oberflaechen darzustellen. Er
 * besitzt die Faehigkeit seinen Inhalt auf dem Bildschirm darzustellen und zu skalieren bzw. aktualisieren. Außerdem
 * besitzt er einen 'nextLayer', also einen naechsten Layer, auf welchen gewechselt werden soll, wenn die
 * entsprechende Variable gesetzt ist (Das eigentliche Wechseln, sowie aktualisieren, redern und skalieren geschieht
 * in Main).
 *
 * @author Tobias Schleifstein
 */
public abstract class Layer {
	/**
	 * Layer in welchen als naechstes gewechselt werden soll.
	 */
	private Layer nextLayer;

	/**
	 * Erstellt einen Layer, ohne naechsten Layer und initialisiert ihn.
	 */
	public Layer() {
		nextLayer = null;

		init();
	}

	/**
	 * Initialisiert den Layer.
	 */
	public abstract void init();

	/**
	 * Aktualisiert den Layer.
	 */
	public abstract void update(long delta);

	/**
	 * Berechnet die Groesse der einzelnen Komponenten eines Layers neu.
	 */
	public abstract void scale();

	/**
	 * Rendert den Layer.
	 */
	public abstract void render();

	/**
	 * Setzt den naechsten Layer.
	 *
	 * @param nextLayer naechster Layer
	 */
	protected void setNextLayer(Layer nextLayer) {
		this.nextLayer = nextLayer;
	}

	/**
	 * Gibt den naechsten Layer zurueck, setzt danach den naechsten Layer auf 'null'.
	 *
	 * @return naechster Layer
	 */
	public Layer getAndRemoveNextLayer() {
		// Speichert den naechsten Layer zwischen und loescht ihn
		Layer temp = nextLayer;
		nextLayer = null;

		// Gibt den naechsten Layer zurueck
		return temp;
	}

	/**
	 * Gibt zurueck, ob es einen naechsten Layer gibt.
	 *
	 * @return Gibt an, ob es einen naechsten Layer gibt
	 */
	public boolean hasNextLayer() {
		return nextLayer != null;
	}
}
