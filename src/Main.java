import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;


/**
 * Main ist die Hauptklasse des Spiels. In dieser wird das gesamte Spiel, sowie alle Layer der graphischen
 * Oberflaeche erstellt und initialisiert. In start() laeuft die Hauptschleife des Spiels, welche kontinuierlich alle
 * GUI Elemente aktualisiert. Die Klasse ist auch fuer das Speichern und Laden der Einstellungen zustaendig.
 *
 * @author Tobias Schleifstein
 */
public class Main {
	/**
	 * Pfad der Save-Datei.
	 */
	public static File saveFile;

	/**
	 * Standardmaessige Einstellung.
	 */
	public static String defaultSave;

	/**
	 * Sehr hellgraue Farbe im Spiel.
	 */
	public static final double VERY_LIGHT_GRAY_R, VERY_LIGHT_GRAY_G, VERY_LIGHT_GRAY_B;

	/**
	 * Hellgraue Farbe im Spiel.
	 */
	public static final double LIGHT_GRAY_R, LIGHT_GRAY_G, LIGHT_GRAY_B;

	/**
	 * Graue Farbe im Spiel.
	 */
	public static final double GRAY_R, GRAY_G, GRAY_B;

	/**
	 * Dunkelgraue Farbe im Spiel.
	 */
	public static final double DARK_GRAY_R, DARK_GRAY_G, DARK_GRAY_B;

	/**
	 * Weisse Farbe im Spiel.
	 */
	public static final double WHITE_R, WHITE_G, WHITE_B;

	static {
		// Save-Datei
		saveFile = new File("save");

		// Standard-Einstellungen festlegen
		defaultSave = "31\n30\n32\n200\n203\n" + "/\n0\n/\n0\n/\n0\n/\n0\n/\n0\n/\n0\n/\n0\n/\n0\n/\n0\n/\n0";

		// Ganz Hellgrau
		VERY_LIGHT_GRAY_R = 0.9;
		VERY_LIGHT_GRAY_G = 0.9;
		VERY_LIGHT_GRAY_B = 0.9;

		// Hellgrau
		LIGHT_GRAY_R = 0.75;
		LIGHT_GRAY_G = 0.75;
		LIGHT_GRAY_B = 0.75;

		// Grau
		GRAY_R = 0.5;
		GRAY_G = 0.5;
		GRAY_B = 0.5;

		// Dunkelgrau/blau
		DARK_GRAY_R = 0.20;
		DARK_GRAY_G = 0.25;
		DARK_GRAY_B = 0.4;

		// Weiss
		WHITE_R = 1;
		WHITE_G = 1;
		WHITE_B = 1;
	}

	/**
	 * Aktueller Layer, welcher angezeigt werden soll.
	 */
	private Layer currentLayer;

	/**
	 * Angestrebte Framerate in frames per second (fps).
	 */
	private int fps;

	/**
	 * Titel des Fensters.
	 */
	private String title;

	/**
	 * Erstellt ein Spiel und ein Fenster.
	 */
	public Main() {
		// Initialisierung
		init();

		// Startet die Spielschleife
		start();
	}

	/**
	 * Initialisiert das Fenster und alle Spielinhalte.
	 */
	private void init() {
		// Display erstellen
		try {
			Display.setDisplayMode(new DisplayMode(720, 720));
			// Display.setFullscreen(true);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}

		// OpenGL initialisieren
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, Display.getWidth(), 0, Display.getHeight(), 1, -1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		// Save-Datei lesen
		load();

		// Layer erstellen und initialisieren
		MenuLayer menuLayer = new MenuLayer();
		GameLayer gameLayer = new GameLayer();
		HighscoresLayer highscoresLayer = new HighscoresLayer();
		ControlsLayer controlsLayer = new ControlsLayer();
		CreditsLayer creditsLayer = new CreditsLayer();

		// Layer-Referenzen zuweisen
		menuLayer.setGameLayer(gameLayer);
		menuLayer.setHighscoresLayer(highscoresLayer);
		menuLayer.setControlsLayer(controlsLayer);
		menuLayer.setCreditsLayer(creditsLayer);

		gameLayer.setHighscoresLayer(highscoresLayer);

		highscoresLayer.setMenuLayer(menuLayer);

		controlsLayer.setMenuLayer(menuLayer);

		creditsLayer.setMenuLayer(menuLayer);

		// Aktuellen Layer setzen
		setCurrentLayer(menuLayer);

		// Angestrebte Framerate festlegen
		fps = 60;

		// Titel des Fensters festlegen
		title = "Tetros";
	}

	/**
	 * Startet eine Schleife, welche die gesamt Laufzeit ueber aktiv ist und das Bild sowie Spiel aktualisiert.
	 */
	public void start() {
		// Variablen zur Berechnung der Zeit seit dem letzten Frame
		long delta, thisFrame, lastFrame;

		// lastFrame vorinitialisieren
		lastFrame = getTime();

		// Laeuft, bis das Fenster geschlossen wird
		while (!Display.isCloseRequested()) {
			// Berechnung der Zeit seit dem letzten Frame
			thisFrame = getTime();
			delta = thisFrame - lastFrame;
			lastFrame = thisFrame;
			// Display.setTitle(String.format("%s | %.2f fps", title, 1d / (delta / 1000d)));
			Display.setTitle(String.format("%s", title));

			// Aktualsieren
			update(delta);

			// Rendern
			render();

			// Fenster refreshen
			Display.update();

			// Pausiert, um auf die angestrebte Framerate zu kommen
			Display.sync(fps);
		}

		// Spiel beenden
		endGame();
	}

	/**
	 * Aktualisiert den aktuellen Layer.
	 *
	 * @param delta Zeit seit dem letzten Frame
	 */
	private void update(long delta) {
		// Aktuellen Layer updaten, wenn es keinen naechsten Layer gibt, sonst
		// wechseln
		if (!currentLayer.hasNextLayer()) {
			currentLayer.update(delta);
		} else {
			setCurrentLayer(currentLayer.getAndRemoveNextLayer());
		}
	}

	/**
	 * Rendert den aktuellen Layer.
	 */
	private void render() {
		currentLayer.render();
	}

	/**
	 * Setzt den aktuellen Layer.
	 *
	 * @param layer Aktueller Layer
	 */
	private void setCurrentLayer(Layer layer) {
		currentLayer = layer;
	}

	/**
	 * Gibt die Zeit in ms zurueck.
	 *
	 * @return Zeit in ms
	 */
	private long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	/**
	 * Beendet das Spiel.
	 */
	public static void endGame() {
		// Speichern
		save();

		// Beenden
		Display.destroy();
		System.exit(0);
	}

	/**
	 * Speichert die Tastenbelegung und die Highscores in res/save.
	 */
	public static void save() {
		try {
			// Writer erstellen
			FileWriter writer = new FileWriter(saveFile);

			// Steuerung speichern
			writer.write(ControlsLayer.keyMoveDown + System.getProperty("line.separator"));
			writer.write(ControlsLayer.keyMoveLeft + System.getProperty("line.separator"));
			writer.write(ControlsLayer.keyMoveRight + System.getProperty("line.separator"));
			writer.write(ControlsLayer.keyRotateLeft + System.getProperty("line.separator"));
			writer.write(ControlsLayer.keyRotateRight + System.getProperty("line.separator"));

			// Highscores speichern
			for (int i = 0; i < HighscoresLayer.highscoresName.length; i++) {
				if (!HighscoresLayer.highscoresName[i].equals(HighscoresLayer.MARKED)) {
					writer.write(HighscoresLayer.highscoresName[i]);
				} else {
					writer.write("NONAME");
				}
				writer.write(System.getProperty("line.separator"));
				writer.write(HighscoresLayer.highscoresValue[i] + System.getProperty("line.separator"));
			}

			// Schreibt in die Datei und schliesst den Writer
			writer.flush();
			writer.close();
		} catch (Exception e) {
			System.err.println("Fehler beim Schreiben in die Save-Datei:");
			e.printStackTrace();
		}
	}

	/**
	 * Laedt die Tastenbelegung und die Highscores aus /save.
	 */
	public static void load() {
		try {
			// Reader erstellen
			BufferedReader bufferedreader = new BufferedReader(new FileReader(saveFile));

			// Steuerung setzen
			ControlsLayer.keyMoveDown = Integer.parseInt(bufferedreader.readLine());
			ControlsLayer.keyMoveLeft = Integer.parseInt(bufferedreader.readLine());
			ControlsLayer.keyMoveRight = Integer.parseInt(bufferedreader.readLine());
			ControlsLayer.keyRotateLeft = Integer.parseInt(bufferedreader.readLine());
			ControlsLayer.keyRotateRight = Integer.parseInt(bufferedreader.readLine());

			// Highscores laden
			HighscoresLayer.highscoresName = new String[10];
			HighscoresLayer.highscoresValue = new int[10];
			for (int i = 0; i < HighscoresLayer.highscoresName.length; i++) {
				HighscoresLayer.highscoresName[i] = bufferedreader.readLine();
				HighscoresLayer.highscoresValue[i] = Integer.parseInt(bufferedreader.readLine());
			}

			// Reader schliessen
			bufferedreader.close();
		} catch (Exception e) {
			System.err.println("Fehler beim Lesen der Save-Datei:");
			e.printStackTrace();

			System.out.println("Neue Save-Datei wird erstellt...");
			try {
				// Writer erstellen
				FileWriter writer = new FileWriter(saveFile);

				// Save-Datei schreiben
				writer.write(defaultSave);

				// Schreibt in die Datei und schliesst den Writer
				writer.flush();
				writer.close();

				System.out.println("Save-Datei wurde erstellt.");

				// Erneut Laden
				load();
			} catch (Exception e2) {
				System.err.println("Fehler beim Erstellen der Save-Datei:");
				e2.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		new Main();
	}
}
