import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;


/**
 * Der MenuLayer stellt das Hauptmenue des Spiels dar. Zunaechst einmal fuehrt er eine Animation aus, bei der er den
 * 'Tetros'-Schriftzug einblendet und nach oben bewegt, sowie die Buttons einblendet. Danach stehen die Buttuns
 * 'PLAY', 'HIGHSCORES', 'CONTROLS', 'CREDITS' und 'EXIT' zur Verfuegung, um in die entsprechenden Layer zu wechseln
 * bze. das Spiel zu beenden.
 *
 * @author Tobias Schleifstein
 */
public class MenuLayer extends Layer {
	/**
	 * Modus, in dem sich das Menue gerade befindet
	 */
	private Mode mode;

	/**
	 * Enthaelt alle Modi in denen sich das Menue befinden kann.
	 */
	private enum Mode {
		TETRIS_FADE_IN, TETRIS_MOVE_UP, BUTTONS_FADE_IN, MENU
	}

	/**
	 * Zeit in ms, die der 'Tetros'-Schriftzug zum Einblenden benoetigt.
	 */
	private long tetrosFadeInTime;

	/**
	 * Alphawert des 'Tetros'-Schriftzuges, welcher sich linear von 0 bis 1 erhoeht.
	 */
	private double tetrosAlpha;

	/**
	 * Zeit in ms, die der 'Tetros'-Schriftzug benoetigt, um sich nach oben zu bewegen.
	 */
	private long tetrosMoveTime;

	/**
	 * Position an der sich der 'Tetros'-Schriftzug gerade befindet. Geht linear von 0 bis 1, wobei 0 die Mitte und 1
	 * die Position am oberen Bilschirmrand repraesentieren.
	 */
	private double tetrosCurrentPosition;

	/**
	 * Position an der sich der 'Tetros'-Schriftzug am Ende befindet.
	 */
	private double tetrosEndPosition;

	/**
	 * Hoehe des 'Tetros'-Schriftzuges.
	 */
	private double tetrosHeight;

	/**
	 * Alphawert der Buttons, welcher sich linear von 0 bis 1 erhoeht.
	 */
	private double buttonAlpha;

	/**
	 * 'Tetros'-Schriftzug in Kachelform.
	 */
	private String[] tetros;

	/**
	 * Zeit in ms, welche die Buttons zum Einblenden benoetigen.
	 */
	private long buttonsFadeInTime;

	/**
	 * 'Play'-Button, um das Spiel zu starten
	 */
	private Button buttonPlay;

	/**
	 * 'Highscores'-Button, um die Highscores anzuzeigen.
	 */
	private Button buttonHighscores;

	/**
	 * 'Controls'-Button, um die Tastenbelegung zu sehen/aendern.
	 */
	private Button buttonControls;

	/**
	 * 'Credits'-Button, um die Credits zu sehen.
	 */
	private Button buttonCredits;

	/**
	 * 'Exit'-Button, um das Spiel zu beenden.
	 */
	private Button buttonExit;

	/**
	 * Array mit allen Buttons.
	 */
	private Button[] buttons;

	/**
	 * Spiel, welches ueber den 'Play'-Button gestartet wird.
	 */
	private GameLayer gameLayer;

	/**
	 * Highscore-Layer, welcher ueber den 'Highscores'-Button erreicht wird.
	 */
	private HighscoresLayer highscoresLayer;

	/**
	 * Controls-Layer, welcher ueber den 'Options'-Button erreicht wird.
	 */
	private ControlsLayer controlsLayer;

	/**
	 * Credits-Layer, welcher ueber den 'Credits'-Button erreicht wird.
	 */
	private CreditsLayer creditsLayer;

	@Override
	public void init() {
		// Setzt den Modus des Menues auf das Einbleden des 'Tetros'-Schriftzuges
		mode = Mode.TETRIS_FADE_IN;

		// Setzt Zeit, die der Schriftzug zum Einblenden braucht
		tetrosFadeInTime = 700;

		// Macht 'Tetros'-Schriftzug zunaechst durchsichtig
		tetrosAlpha = 0;

		// Initialisiert 'Tetros'-Schriftzug
		tetros = new String[]{
				" 00000 0000 00000 0000 0000 0000 ",
				" 0 0 0 0  0 0 0 0 0  0 0  0 0  0 ",
				"   0   0      0   0  0 0  0 0    ",
				"   0   00     0   0000 0  0 0000 ",
				"   0   0      0   0 0  0  0    0 ",
				"   0   0  0   0   0  0 0  0 0  0 ",
				"  000  0000  000  0  0 0000 0000 "};

		// Setzt Bewegungsgeschwindigkeit des Schriftzuges
		tetrosMoveTime = 500;

		// Setzt den 'Tetros'-Schriftzug zunaechst in die Mitte
		tetrosCurrentPosition = 0;

		// Setzt Zeit, die die Buttons zum Einblenden brauchen
		buttonsFadeInTime = 500;

		// Macht die Buttons zunaechst durchsichtig
		buttonAlpha = 0;

		// Initialisiert die Buttons
		buttonPlay = new Button();
		buttonPlay.setTitle("PLAY");

		buttonHighscores = new Button();
		buttonHighscores.setTitle("HIGHSCORES");

		buttonControls = new Button();
		buttonControls.setTitle("CONTROLS");

		buttonCredits = new Button();
		buttonCredits.setTitle("CREDITS");

		buttonExit = new Button();
		buttonExit.setTitle("EXIT");

		buttons = new Button[5];
		buttons[0] = buttonPlay;
		buttons[1] = buttonHighscores;
		buttons[2] = buttonControls;
		buttons[3] = buttonCredits;
		buttons[4] = buttonExit;

		// Alles richtig skalieren
		scale();

		// Tastaturspeicher leeren
		while (Keyboard.next()) {
			// nichts
		}
	}

	@Override
	public void update(long delta) {
		// Animation ueberspringen, wenn eine Taste gedrueckt wird
		boolean animation = true;
		if (Keyboard.getNumKeyboardEvents() != 0) {
			animation = false;
		}

		// Entsprechend auf Modus reagieren
		switch (mode) {
			case TETRIS_FADE_IN:
				// Transparanz bis 1 verringern
				if (tetrosAlpha < 1 && animation) {
					tetrosAlpha += 1.0 * delta / tetrosFadeInTime;
				} else {
					// In den naechsten Modus schalten
					tetrosAlpha = 1;
					mode = Mode.TETRIS_MOVE_UP;
				}
				break;

			case TETRIS_MOVE_UP:
				// 'Tetros'-Schriftzug bewegen
				if (tetrosCurrentPosition < 1 && animation) {
					tetrosCurrentPosition += 1.0 * delta / tetrosMoveTime;
				} else {
					// In den naechsten Modus schalten
					tetrosCurrentPosition = 1;
					mode = Mode.BUTTONS_FADE_IN;
				}
				break;

			case BUTTONS_FADE_IN:
				// Transparanz bis 1 verringern
				if (buttonAlpha < 1 && animation) {
					buttonAlpha += 1d * delta / buttonsFadeInTime;
				} else {
					// In den naechsten Modus schalten
					buttonAlpha = 1;
					for (int i = 0; i < buttons.length; i++) {
						buttons[i].setPressable(true);
					}
					mode = Mode.MENU;
				}
				break;

			case MENU:
				// Buttons updaten
				for (int i = 0; i < buttons.length; i++) {
					buttons[i].update();
				}

				// Auf Buttons reagieren
				if (buttonPlay.isClicked()) {
					// Menue beenden und zum Spiel wechseln
					setNextLayer(gameLayer);
				}

				if (buttonHighscores.isClicked()) {
					// Menue beenden und zu den Highscores wechseln
					setNextLayer(highscoresLayer);
				}

				if (buttonControls.isClicked()) {
					// Menue beenden und zu den Optionen wechseln
					setNextLayer(controlsLayer);
				}

				if (buttonCredits.isClicked()) {
					// Menue beenden und zu den Credits wechseln
					setNextLayer(creditsLayer);
				}

				if (buttonExit.isClicked()) {
					// Spiel beenden
					Main.endGame();
				}

				// Tastaturspeicher leeren
				while (Keyboard.next()) {
					// nichts
				}
				break;
		}

		// Transparanz der Buttons setzen
		for (int i = 0; i < buttons.length; i++) {
			buttons[i].setAlpha(buttonAlpha);
		}
	}

	@Override
	public void scale() {
		// Unterteilt das Display in Reihen an denen sich die GUI Elemente orientieren
		double l = Display.getHeight() / 15d;

		// Hoehe des Tetros Schriftzuges
		tetrosHeight = 3 * l;

		// Hoehe
		tetrosEndPosition = Display.getHeight() - 1 * l;

		for (int i = 0; i < buttons.length; i++) {
			// Setzt die Buttons
			buttons[i].setBounds(0.2 * Display.getWidth(), tetrosEndPosition - (2 * i + 5) * l,
					0.8 * Display.getWidth(), tetrosEndPosition - (2 * i + 4) * l);
			buttons[i].setAlpha(buttonAlpha);
			buttons[i].setPressable(false);
		}
	}

	@Override
	public void render() {
		// Hintergrund
		GL11.glColor3d(Main.VERY_LIGHT_GRAY_R, Main.VERY_LIGHT_GRAY_G, Main.VERY_LIGHT_GRAY_B);
		GL11.glRectd(0, 0, Display.getWidth(), Display.getHeight());

		// 'Tetros'-Schriftzug

		// Breite einer Kachel auf dem Bildschirm
		double width = tetrosHeight / tetros.length;

		// Koordinaten fuer mittige Ausrichtung des Schriftzuges
		double midX = (Display.getWidth() - tetros[0].length() * width) * 0.5;
		double midY = (Display.getHeight() + tetros.length * width) * 0.5;

		// Aktuelle tatsaechliche Position des 'Tetros'-Schriftzuges mit sinusfoermigem Verlauf
		double currentPosition = midY - (midY - tetrosEndPosition) * Math.sin(tetrosCurrentPosition * 90.0 / 180 *
				Math.PI);

		// Zeichnet die Kacheln des Schriftzuges
		GL11.glColor4d(Main.DARK_GRAY_R, Main.DARK_GRAY_G, Main.DARK_GRAY_B, tetrosAlpha);
		for (int i = 0; i < tetros.length; i++) {
			for (int j = 0; j < tetros[i].length(); j++) {
				if (tetros[i].charAt(j) == '0') {
					// Zeichnet die Kachel
					GL11.glRectd(midX + j * width, currentPosition - i * width, midX + (j + 1) * width - 1,
							currentPosition - (i + 1) * width + 1);
				}
			}
		}

		// Buttons
		for (int i = 0; i < buttons.length; i++) {
			buttons[i].render();
		}
	}

	/**
	 * Setzt das Spiel, welches ueber den 'Play'-Button gestartet wird.
	 *
	 * @param gameLayer Spiel, welches ueber den 'Play'-Button gestartet wird.
	 */
	public void setGameLayer(GameLayer gameLayer) {
		this.gameLayer = gameLayer;
	}

	/**
	 * Setzt den Highscore-Layer, welcher ueber den 'Highscores'-Button erreicht
	 * wird.
	 *
	 * @param highscoresLayer Highscore-Layer, welcher ueber den 'Highscores'-Button erreicht
	 *                        wird.
	 */
	public void setHighscoresLayer(HighscoresLayer highscoresLayer) {
		this.highscoresLayer = highscoresLayer;
	}

	/**
	 * Setzt den Controls-Layer, welcher ueber den 'Options'-Button erreicht
	 * wird.
	 *
	 * @param controlsLayer Controls-Layer, welcher ueber den 'Options'-Button erreicht
	 *                      wird.
	 */
	public void setControlsLayer(ControlsLayer controlsLayer) {
		this.controlsLayer = controlsLayer;
	}

	/**
	 * Setzt den Credits-Layer, welcher ueber den 'Credits'-Button erreicht wird.
	 *
	 * @param creditsLayer Credits-Layer, welcher ueber den 'Credits'-Button erreicht
	 *                     wird.
	 */
	public void setCreditsLayer(CreditsLayer creditsLayer) {
		this.creditsLayer = creditsLayer;
	}
}
